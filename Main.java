public class Main {
    public static void main(String[] args) {

        int[][] xadrez = new int[8][8];

        xadrez[0][0] = 4;
        xadrez[0][1] = 3;
        xadrez[0][2] = 2;
        xadrez[0][3] = 5;
        xadrez[0][4] = 6;
        xadrez[0][5] = 2;
        xadrez[0][6] = 3;
        xadrez[0][7] = 4;

        xadrez[1][0] = 1;
        xadrez[1][1] = 1;
        xadrez[1][2] = 1;
        xadrez[1][3] = 1;
        xadrez[1][4] = 1;
        xadrez[1][5] = 1;
        xadrez[1][6] = 1;
        xadrez[1][7] = 1;

        xadrez[2][0] = 0;
        xadrez[2][1] = 0;
        xadrez[2][2] = 0;
        xadrez[2][3] = 0;
        xadrez[2][4] = 0;
        xadrez[2][5] = 0;
        xadrez[2][6] = 0;
        xadrez[2][7] = 0;

        xadrez[3][0] = 0;
        xadrez[3][1] = 0;
        xadrez[3][2] = 0;
        xadrez[3][3] = 0;
        xadrez[3][4] = 0;
        xadrez[3][5] = 0;
        xadrez[3][6] = 0;
        xadrez[3][7] = 0;

        xadrez[4][0] = 0;
        xadrez[4][1] = 0;
        xadrez[4][2] = 0;
        xadrez[4][3] = 0;
        xadrez[4][4] = 0;
        xadrez[4][5] = 0;
        xadrez[4][6] = 0;
        xadrez[4][7] = 0;

        xadrez[5][0] = 0;
        xadrez[5][1] = 0;
        xadrez[5][2] = 0;
        xadrez[5][3] = 0;
        xadrez[5][4] = 0;
        xadrez[5][5] = 0;
        xadrez[5][6] = 0;
        xadrez[5][7] = 0;

        xadrez[6][0] = 1;
        xadrez[6][1] = 1;
        xadrez[6][2] = 1;
        xadrez[6][3] = 1;
        xadrez[6][4] = 1;
        xadrez[6][5] = 1;
        xadrez[6][6] = 1;
        xadrez[6][7] = 1;

        xadrez[7][0] = 4;
        xadrez[7][1] = 3;
        xadrez[7][2] = 2;
        xadrez[7][3] = 5;
        xadrez[7][4] = 6;
        xadrez[7][5] = 2;
        xadrez[7][6] = 3;
        xadrez[7][7] = 4;

        int contador1 = 0;
        int contador2 = 0;
        int contador3 = 0;
        int contador4 = 0;
        int contador5 = 0;
        int contador6 = 0;


        for (int i = 0; i < xadrez.length; i++) {
            for (int j = 0; j < xadrez[i].length; j++) {
                System.out.print(xadrez[i][j] + " ");
            }
            System.out.println("");
        }

        for (int i = 0; i < xadrez.length; i++) {
            for (int j = 0; j < xadrez[i].length; j++) {
                while (xadrez[i][j] == 1){
                    contador1++; break;
                } while (xadrez[i][j] == 2) {
                    contador2++; break;
                } while (xadrez[i][j] == 3) {
                    contador3++; break;
                } while (xadrez[i][j] == 4) {
                    contador4++; break;
                } while (xadrez[i][j] == 5) {
                    contador5++; break;
                } while (xadrez[i][j] == 6) {
                    contador6++; break;
                }
            }
        }

        System.out.println("");
        System.out.println("Peão: " + contador1 + " peça(s)");
        System.out.println("Bispo: " + contador2 + " peça(s)");
        System.out.println("Cavalo: " + contador3 + " peça(s)");
        System.out.println("Torre: " + contador4 + " peça(s)");
        System.out.println("Rainha: " + contador6 + " peça(s)");
        System.out.println("Rei: " + contador5 + " peça(s)");
    }
}
